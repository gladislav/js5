// 1.Метод об'єкту - це функція для взаємодії з об'єктом.Вона виконує якусь дію з обєктом або повертає результат.
// 2.Значення властивості об'єкта можуть мати будь-який тип даних  а також інший об'єкт.
// 3.Об'єкт є посилальним типом даних, що означає, що коли ми присвоюємо змінній значення об'єкта, то змінна не 
//   отримує копію об'єкта, а лише посилання на нього. 

function createNewUser() {
  let firstName = prompt("Enter your first name:");
  let lastName = prompt("Enter your last name:");
  let newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function() {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    }
  };
  return newUser;
}
let user = createNewUser();
console.log(user.getLogin());

